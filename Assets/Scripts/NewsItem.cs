﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewsItem : MonoBehaviour {

    [TextArea(3, 10)]
    public string title;
    [TextArea(3, 12)]
    public string body;
    public int money;
    public int reputation;
    public bool longNewsItem;
    public int dailyImage;
    public Categories category;
    public bool secret;
}
