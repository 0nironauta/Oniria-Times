﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyPicture : MonoBehaviour {

    public Sprite[] picturesDay1;
    public Sprite[] picturesDay2;
    public Sprite[] picturesDay3;
    public Sprite[] picturesDay4;
    public Sprite[] picturesDay5;
    public Sprite[] picturesDay6;
    public Sprite[] picturesDay7;

    public Image picture;

    public int selectedPicture;

    public void SetPicture(int day)
    {
        switch (day)
        {
            case 1:
                selectedPicture = Random.Range(0, picturesDay1.Length );
                picture.sprite = picturesDay1[selectedPicture];
                break;
            case 2:
                selectedPicture = Random.Range(0, picturesDay2.Length );
                picture.sprite = picturesDay2[selectedPicture];
                break;
            case 3:
                selectedPicture = Random.Range(0, picturesDay3.Length);
                picture.sprite = picturesDay3[selectedPicture];
                break;
            case 4:
                selectedPicture = Random.Range(0, picturesDay4.Length);
                picture.sprite = picturesDay4[selectedPicture];
                break;
            case 5:
                selectedPicture = Random.Range(0, picturesDay5.Length);
                picture.sprite = picturesDay5[selectedPicture];
                break;
            case 6:
                selectedPicture = Random.Range(0, picturesDay6.Length);
                picture.sprite = picturesDay6[selectedPicture];
                break;
            case 7:
                selectedPicture = Random.Range(0, picturesDay7.Length);
                picture.sprite = picturesDay7[selectedPicture];
                break;
        }
    }
}
