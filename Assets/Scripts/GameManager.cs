﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;

public class GameManager : MonoBehaviour {

    public GameObject newsPrefab;
    public GameObject newsListPanel;
    public GameObject editorBossPanel;
    public GameObject editorNewsPanel;
    public GameObject publishPanel;
    public GameObject gameOverPanel;
    
    public Transform leftColumn;
    public Transform rightColumn;
    public Transform centralColumn;

    public Button centralColumnEditor;
    public Button chooseNewsButton;
    public Button publishButton;
    public Button inspectSales;

    public Text number;
    public Text day;
    public Text price;

    public string[] numbers;
    public string[] days;

    public Text publishedNewsText;

    public int currentDay = 1;
    News news;
    NewsItem selectedNewsItem;
    NewsItem leftNews;
    NewsItem rightNews;
    NewsItem centerNews;

    bool columnLeft_filled;
    bool columnRight_filled;
    bool columnCenter_filled;
    bool activatedPublishPanelOnce;

    public List<NewsItem> publishedNews = new List<NewsItem>();

    void Start () {

        GetComponent<DailyPicture>().SetPicture(currentDay);

        news = GetComponent<News>();

        GetDailyNews();

	}

    void GetDailyNews()
    {
        List<NewsItem> dailyNews = news.LoadDailyNews(currentDay);

        foreach (NewsItem news in dailyNews)
        {
            if (!news.secret || news.secret && currentDay==4 && GetComponent<Plot>().day4SecretFound)
            {
                GameObject instantiatedNewsItem = Instantiate(newsPrefab);
                instantiatedNewsItem.transform.SetParent(newsListPanel.transform);
                instantiatedNewsItem.transform.GetChild(0).GetComponent<Text>().text = news.title;
                instantiatedNewsItem.transform.GetChild(1).GetComponent<Text>().text = news.body;
                instantiatedNewsItem.GetComponent<Button>().onClick.AddListener(delegate { SelectSlot(news); });
            }
        }
    }
	
    void SelectSlot(NewsItem newsItem)
    {
        editorBossPanel.SetActive(true);
        if (newsItem.longNewsItem)
        {
            centralColumnEditor.interactable = false;
        }
        else
        {
            centralColumnEditor.interactable = true;
        }

        selectedNewsItem = newsItem;
    }

    public void ColumnSelected(int index)
    {
        selectedNewsItem.title = selectedNewsItem.title.ToUpper();
        switch (index)
        {
            case 0:
                leftColumn.GetChild(0).GetComponent<Text>().text = selectedNewsItem.title;
                leftColumn.GetChild(1).GetComponent<Text>().text = selectedNewsItem.body;
                columnLeft_filled = true;
                leftNews = selectedNewsItem;
                break;
            case 2:
                rightColumn.GetChild(0).GetComponent<Text>().text = selectedNewsItem.title;
                rightColumn.GetChild(1).GetComponent<Text>().text = selectedNewsItem.body;
                columnRight_filled = true;
                rightNews = selectedNewsItem;
                break;
            case 1:
                centralColumn.GetChild(0).GetComponent<Text>().text = selectedNewsItem.title;
                centralColumn.GetChild(1).GetComponent<Text>().text = selectedNewsItem.body;
                columnCenter_filled = true;
                centerNews = selectedNewsItem;
                break;
        }

        editorNewsPanel.SetActive(false);
        editorBossPanel.SetActive(false);
        chooseNewsButton.interactable = true;

        GetComponent<AudioManager>().PlayWriteNews();

        if(columnLeft_filled && columnRight_filled && columnCenter_filled && !activatedPublishPanelOnce)
        {
            publishPanel.SetActive(true);
            publishButton.interactable = true;
            activatedPublishPanelOnce = true;
            GetComponent<AudioManager>().PlayOutOfSpace();
        }

        
    }

    public void Publish()
    {
        //print(centerNews.title);

        if (leftNews.title == "OUR INTELLITUNA LORDS FORGIVE US" ||
            rightNews.title == "OUR INTELLITUNA LORDS FORGIVE US" ||
            centerNews.title == "OUR INTELLITUNA LORDS FORGIVE US")
        {
            //print("tunaVictory");
            GetComponent<Plot>().tunaVictory = true;
        }

        GetComponent<Balance>().CalculateBalance(leftNews, rightNews, centerNews);

        publishedNews.Add(leftNews);
        publishedNews.Add(rightNews);
        publishedNews.Add(centerNews);

        GetComponent<AudioManager>().PlaynewspaperLaunch();
        Invoke("ActivateButtonInspectSales",1.25F);
    }

    public void ActivateButtonInspectSales()
    {
        inspectSales.gameObject.SetActive(true);
        GameObject.Find("OniriaTimes").GetComponent<Animator>().enabled = false;
        GameObject.Find("OniriaTimes").GetComponent<Animator>().Play("PublishNewspaper",-1,0);
    }

    public void NextPublication()
    {

        if (currentDay == 7) GameOver(true);
        else
        {
            // Empty news
            leftColumn.GetChild(0).GetComponent<Text>().text = "";
            leftColumn.GetChild(1).GetComponent<Text>().text = "";
            rightColumn.GetChild(0).GetComponent<Text>().text = "";
            rightColumn.GetChild(1).GetComponent<Text>().text = "";
            centralColumn.GetChild(0).GetComponent<Text>().text = "";
            centralColumn.GetChild(1).GetComponent<Text>().text = "";

            // Reset variables
            selectedNewsItem = null;
            leftNews = null;
            rightNews = null;
            centerNews = null;

            columnLeft_filled = false;
            columnRight_filled = false;
            columnCenter_filled = false;

            activatedPublishPanelOnce = false;

            foreach (Transform child in newsListPanel.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            // Advence day
            currentDay++;
            number.text = numbers[currentDay - 1].ToString();
            day.text = days[currentDay - 1].ToString();
            GetComponent<DailyPicture>().SetPicture(currentDay);
            GetDailyNews();
        }
    }

    public void GameOver(bool win)
    {
        gameOverPanel.SetActive(true);
        GetComponent<Balance>().GameOverBalance();
        GetComponent<AudioManager>().GameOver(win);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowPublishedNews()
    {

        if (publishedNews.Count == 0)
        {
            publishedNewsText.text = "you have not published any news yet";
        }
        else
        {
            publishedNewsText.text = "";
            foreach (NewsItem item in publishedNews)
            {
                publishedNewsText.text += "-" + item.title + "\n";
            }
        }      
    }
}
