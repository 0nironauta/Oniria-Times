﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plot : MonoBehaviour {

    public string[] plots;
    public Sprite[] images;
    public Text bocadillo;
    public GameObject accept;
    public GameObject deny;
    public GameObject ok;
    public Image emote;

    public bool day4SecretFound;
    public bool tunaVictory;

    int currentPlot;

    public void NextPlot()
    {
        if (GetComponent<Balance>().balanceTotal < 0) GetComponent<GameManager>().GameOver(false);
        else
        {

            int day = GetComponent<GameManager>().currentDay - 1;

            bocadillo.text = plots[day];

            switch (day)
            {
                case 0:
                    GetComponent<Balance>().maintenance = 5000;
                    GetComponent<Balance>().maintenanceText.text = "-5000A";
                    break;

                case 1:
                    accept.SetActive(true);
                    deny.SetActive(true);
                    ok.SetActive(false);
                    emote.sprite = images[1];
                    break;
                case 2:
                    emote.sprite = images[2];
                    break;
                case 3:
                    emote.sprite = images[0];
                    accept.SetActive(false);
                    deny.SetActive(false);
                    ok.SetActive(true);
                    break;
                case 4:
                    emote.sprite = images[3];
                    accept.SetActive(true);
                    deny.SetActive(true);
                    ok.SetActive(false);
                    GetComponent<Balance>().maintenance = 10000;
                    GetComponent<Balance>().maintenanceText.text = "-7500A";
                    break;
                case 5:
                    emote.sprite = images[1];
                    accept.SetActive(false);
                    deny.SetActive(false);
                    ok.SetActive(true);
                    ok.GetComponentInChildren<Text>().text = "Leave me alone...";
                    break;
                case 6:
                    if (tunaVictory)
                    {
                        emote.sprite = images[5];
                        bocadillo.text = plots[day + 1];
                        ok.GetComponentInChildren<Text>().text = "yes, master";
                    }
                    else
                    {
                        emote.sprite = images[4];
                        ok.GetComponentInChildren<Text>().text = "whatever";
                    }
                    break;
            }
        }

    }

    public void Accept()
    {
        int day = GetComponent<GameManager>().currentDay-1;

        switch (day)
        {
            case 1:
                GetComponent<Balance>().price += 0.10F;
                GetComponent<Balance>().priceText.text = GetComponent<Balance>().price+" A";
                GetComponent<Balance>().sellingsFactor -= Random.Range(25,125);
                GetComponent<GameManager>().price.text = "Price: "+ GetComponent<Balance>().price + " A";
                break;
            case 2:             
                GetComponent<Balance>().acumulatedReputation++;
                break;
            case 4:
                GetComponent<Balance>().price += 0.10F;
                GetComponent<Balance>().priceText.text = GetComponent<Balance>().price + " A";
                GetComponent<Balance>().sellingsFactor -= Random.Range(25, 125);
                GetComponent<GameManager>().price.text = "Price: " + GetComponent<Balance>().price + " A";
                break;
        }
    }

    public void Deny()
    {
        int day = GetComponent<GameManager>().currentDay - 1;

        switch (day)
        {
            case 2:
                day4SecretFound = true;
                break;
        }
    }
}
