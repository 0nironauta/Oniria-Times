﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Balance : MonoBehaviour {

    public float sold;
    public float soldtotal;
    public float bonus;
    public float bonusTotal;
    public float total;
    public float price = 0.80F;
    public float earnings;
    public float earningsTotal;
    public float maintenance = 4000;
    public float balance;
    public float balanceTotal;
    public float reputation;
    public float acumulatedReputation;

    public Text soldText;
    public Text bonusText;
    public Text totalText;
    public Text priceText;
    public Text earningsText;
    public Text maintenanceText;
    public Text balanceText;
    public Text reputationText;

    public Text statusEarnings;
    public Text statusReputation;

    public Image emote;
    public Sprite[] emotes;
    public Text analisisText;
    public string[] analisis;

    public float sellingsFactor = 1000;

    public Text GameOverSoldNewspapers;
    public Text GameOverTotalEarningsText;
    public Text GameOverTotalEarnings;
    public Text GameOverReputation;
    public Text GameOverSecret;
    public Text GameOverTuna;
    public Text GameOverSurvive;

    public Text science;
    public Text literature;
    public Text geography;
    public Text history;
    public Text society;
    public Text sports;
    public Text events;


    public void CalculateBalance(NewsItem left, NewsItem right, NewsItem center)
    {
        sold = (left.money + right.money + center.money) * (sellingsFactor+acumulatedReputation*100);
        if(left.title == right.title || left.title == center.title || center.title == right.title)
        {
            sold /= sellingsFactor;
        }
        if(!left.longNewsItem || !right.longNewsItem)
        {
            sold /= 2;
        }
        soldtotal += sold;
        soldText.text = sold.ToString();

        if (center.dailyImage == GetComponent<DailyPicture>().selectedPicture)
        {
            bonus = (center.money * sellingsFactor) + sellingsFactor;
            bonusTotal += bonus;
            bonusText.text = bonus.ToString();
        }

        total = sold + bonus;
        totalText.text = total.ToString();

        earnings = total * price;
        earningsTotal += earnings;
        earningsText.text = earnings.ToString() + "A";

        balance = earnings - maintenance;
        balanceTotal += balance;
        balanceText.text = balance.ToString() + "A";
        if (balance < 0)
        {
            balanceText.color = Color.red;
        }
        else
        {
            balanceText.color = Color.white;
        }

        reputation = left.reputation + right.reputation + center.reputation;
        if (left.title == right.title || left.title == center.title || center.title == right.title)
        {
            reputation = -1;
        }
        acumulatedReputation += reputation;
        
        if (reputation > 0)
        {
            reputationText.text = "+ "+ reputation.ToString();
            reputationText.color = Color.green;
        }
        if (reputation < 0)
        {
            reputationText.text = reputation.ToString();
            reputationText.color = Color.red;
        }

        if (reputation == 0)
        {
            reputationText.text = reputation.ToString();
            reputationText.color = Color.white;
        }

        statusEarnings.text = "EARNINGS: "+balanceTotal.ToString()+"A";
        statusReputation.text = "REPUTATION: "+acumulatedReputation.ToString();
        if (balanceTotal < 0)
        {
            statusEarnings.color = Color.red;
        }
        else
        {
            statusEarnings.color = Color.white;
        }

        if (acumulatedReputation < 0)
        {
            statusReputation.color = Color.red;
        }
        else
        {
            statusReputation.color = Color.white;
        }
    }

    public void Analysis()
    {
        if(reputation < 0)
        {
            emote.sprite = emotes[2];
            analisisText.text = analisis[2];
            GetComponent<AudioManager>().PlayPhoneRing();
        }
        else
        {
            if(balance < 0)
            {
                emote.sprite = emotes[1];
                analisisText.text = analisis[1];
                GetComponent<AudioManager>().PlayLowCash();
            }
            else
            {
                if (balance > 0)
                {

                    emote.sprite = emotes[0];
                    analisisText.text = analisis[0];
                    GetComponent<AudioManager>().PlayApplause();
                }
                else
                {
                    if (balance == 0)
                    {
                        emote.sprite = emotes[2];
                        analisisText.text = "We Should make more money";
                    }
                }
            }
        }
    }

    public void GameOverBalance()
    {
        GameOverSoldNewspapers.text = (soldtotal + bonusTotal).ToString();
        if (balanceTotal < 0)
        {
            GameOverTotalEarningsText.text = "Total earnings (bankruptcy)";
            GameOverTotalEarningsText.color = Color.red;
            GameOverSurvive.gameObject.SetActive(true);
        }
        GameOverTotalEarnings.text = balanceTotal.ToString();
        GameOverReputation.text = acumulatedReputation.ToString();

        if (GetComponent<Plot>().day4SecretFound)
        {
            GameOverSecret.text = "Yes";
        }

        if (GetComponent<Plot>().tunaVictory)
        {
            GameOverTuna.text = "Yes (shame!)";
        }

        int iscience   =0;
        int iliterature=0;
        int igeography =0;
        int ihistory   =0;
        int isociety   =0;
        int isports    =0;
        int ievents    =0;

        foreach (NewsItem item in GetComponent<GameManager>().publishedNews)
        {
            switch (item.category)
            {
                case Categories.SCIENCIE:
                    iscience++;
                    break;
                case Categories.LITERATURE:
                    iliterature++;
                    break;
                case Categories.GEOGRAPHY:
                    igeography++;
                    break;
                case Categories.HISTORY:
                    ihistory++;
                    break;
                case Categories.SOCIETY:
                    isociety++;
                    break;
                case Categories.SPORTS:
                    isports++;
                    break;
                case Categories.EVENTS:
                    ievents++;
                    break;

            }
        }

        science.text    = "Science news: "    + iscience.ToString();
        literature.text = "Literature news: " + iliterature.ToString();
        geography.text  = "Geography news: "  + igeography.ToString();
        history.text    = "History news: "    + ihistory.ToString();
        society.text    = "Society news: "    + isociety.ToString();
        sports.text     = "Sport news: "      + isports.ToString();
        events.text     = "Events news: "     + ievents.ToString();
    }
}
