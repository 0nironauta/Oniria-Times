﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class News : MonoBehaviour {

    public GameObject newsObject;

    public List<NewsItem> LoadDailyNews(int day)
    {
        List<NewsItem> news = new List<NewsItem>();
        Transform dayNews = newsObject.transform.GetChild(day-1);
        news = dayNews.gameObject.GetComponentsInChildren<NewsItem>().ToList<NewsItem>();

        return news;
    }
}
