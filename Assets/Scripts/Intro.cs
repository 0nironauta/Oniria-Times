﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour {

    int step = 0;
    public GameObject welcome;
    public GameObject land;
    public GameObject you;
    public GameObject you2;
    public GameObject choose;
    public GameObject days;


    public void Next()
    {
        switch (step)
        {
            case 0:
                land.SetActive(true);
                step++;
                break;
            case 1:
                welcome.SetActive(false);
                you.SetActive(true);
                step++;
                break;
            case 2:
                you2.SetActive(false);
                choose.SetActive(true);
                step++;
                break;
            case 3:
                choose.SetActive(false);
                days.SetActive(true);
                step++;
                break;
            case 4:
                GameObject.Find("GameManager").GetComponent<AudioManager>().PlayMusic();
                gameObject.SetActive(false);
                break;
        }
    }
}
