﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    public AudioClip oniriaTimes;
    public AudioClip sadSaxo;
    public AudioClip win;
    public AudioClip newspaperLaunch;
    public AudioClip writeNews;
    public AudioClip applause;
    public AudioClip lowCash;
    public AudioClip phoneRing;
    public AudioClip outOfSpace;

    public Sprite audioOn;
    public Sprite audioOff;
    public Button audioButton;

    public AudioListener listener;

    public AudioSource music;
    public AudioSource fx;

    public void PlayMusic()
    {
        music.Play();
    }

    public void PlayWriteNews()
    {
        fx.Stop();
        fx.clip = writeNews;
        fx.Play();
    }

    public void PlaynewspaperLaunch()
    {
        music.volume = 0.25f;
        fx.Stop();
        fx.clip = newspaperLaunch;
        fx.Play();
        Invoke("ResetVolume", 3);
    }

    public void PlayApplause()
    {
        fx.Stop();
        fx.clip = applause;
        fx.Play();
    }

    public void PlayLowCash()
    {
        fx.Stop();
        fx.clip = lowCash;
        fx.Play();
    }

    public void PlayPhoneRing()
    {
        fx.Stop();
        fx.clip = phoneRing;
        fx.Play();
    }

    public void PlayOutOfSpace()
    {
        fx.Stop();
        fx.clip = outOfSpace;
        fx.Play();
    }

    public void AudioOnOff()
    {
        if (listener.enabled)
        {
            listener.enabled = false;
            audioButton.GetComponent<Image>().sprite = audioOff;
        }
        else
        {
            listener.enabled = true;
            audioButton.GetComponent<Image>().sprite = audioOn;

        }
    }

    public void GameOver(bool winner)
    {
        music.Stop();
        if (winner) music.clip = win;
        else music.clip = sadSaxo;
        music.loop = false;
        music.Play();
    }

    void ResetVolume()
    {
        music.volume = 1;
    }


}
